<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Phpfastcache\Helper\Psr16Adapter;

use App\Account;
use App\Comment;
use App\Post;
use App\Exports\CommentsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Crypt;

class PostController extends Controller
{
    private function accountReset(){
        $accounts = Account::all()->count();
        $in_use = Account::where('in_use', 1)->get()->count();
        if($in_use == $accounts){
            $allAccounts = Account::all();
            foreach($allAccounts as $oneAccount){
                $oneAccount->update(['in_use' => 0]);
            }
        }
    }

    private function decryptHash($hash)
    {
        if (empty($hash)) return [];

        $decrypted = Crypt::decryptString($hash); // array

        $result = explode('..', $decrypted);
        $data = [
            'email'     => $result[0] ?? '',
            'password'  => $result[1] ?? '',
            'date'      => $result[2] ?? ''
        ];

        return $data['password'];
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->q) AND $request->q != ""){
            $query = Post::query();        
            $columns = Schema::getColumnListing('posts');
            
            foreach($columns as $column)
            {
                $query->orWhere($column, 'LIKE', '%'.$request->q.'%');
            }

            $posts = $query->orderBy('id','DESC')->paginate();
            return view('post.index',['posts' => $posts]);
        }
        
        $posts = Post::orderBy('id','desc')->paginate();

        foreach ($posts as $post)
            $post->commentsCount = Comment::where('post_id', $post->id)->count();

        return view('post.index',['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $instagramAccount = Account::where('in_use', 0)->first();
        $instagramAccount->update(['in_use' => 1]);
        $this->accountReset();

        $username = $instagramAccount['email'];
        $password = $this->decryptHash($instagramAccount['password']);
        
        $instagram = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), $username, $password, new Psr16Adapter('Files'));

        $instagram->login(); // will use cached session if you want to force login $instagram->login(true)
        $instagram->saveSession(); //DO NOT forget this in order to save the session, otherwise have no sense

        $response = $instagram->getPaginateMediaCommentsByCode($request->instagram_post_code, 1, null);
        $totalComments = $response->commentsCount;

        Post::create([
            'name'                  => $request->name,
            'instagram_post_code'   => $request->instagram_post_code,
            'sleep_time'            => ($request->sleep_time * 60),
            'count'                 => $request->count,
            'total'                 => $totalComments,
            'max_id'                => $request->max_id,
            'paginateInd'           => $request->paginateInd,
        ]);

        $request->session()->flash('status', 'Post cadastrado com sucesso!');
        return redirect()->to(route('posts.index'));
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::withTrashed()->findOrFail($id);
        $data = "<table class='table table-bordered table-striped table-hover' width='100%'>";
        $data .= "<tr><td><b>Post</b></td><td>".$post->name."</td></tr>";
        $data .= "<tr><td><b>Code</b></td><td>".$post->instagram_post_code."</td></tr>";
        $data .= "<tr><td><b>Pausa</b></td><td>".$post->sleep_time."</td></tr>";
        $data .= "<tr><td><b>Quantidade</b></td><td>".$post->count."</td></tr>";
        $data .= "<tr><td><b>Maior ID</b></td><td>".$post->total."</td></tr>";
        $data .= "<tr><td><b>Criado</b></td><td>".$post->created_at->format('d/m/Y H:i:s')."</td></tr>";
        $data .= "</table>";

        return $data;
        ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('post.edit',['post' => $post]);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instagramAccount = Account::where('in_use', 0)->first();
        $instagramAccount->update(['in_use' => 1]);
        $this->accountReset();

        $username = $instagramAccount['email'];
        $password = $this->decryptHash($instagramAccount['password']);
        
        $instagram = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), $username, $password, new Psr16Adapter('Files'));

        $instagram->login(); // will use cached session if you want to force login $instagram->login(true)
        $instagram->saveSession(); //DO NOT forget this in order to save the session, otherwise have no sense

        $response = $instagram->getPaginateMediaCommentsByCode($request->instagram_post_code, 1, null);
        $totalComments = $response->commentsCount;

        $post = Post::findOrFail($id);
        
        $username = $request->username;
        $password = $request->password;

        $post->update([
            'name'                  => $request->name,
            'instagram_post_code'   => $request->instagram_post_code,
            'sleep_time'            => ($request->sleep_time * 60),
            'count'                 => $request->count,
            'total'                 => $totalComments,
            'max_id'                => $request->max_id,
            'paginateInd'           => $request->paginateInd,
        ]);

        $request->session()->flash('status', 'Post atualizado com sucesso!');
        return redirect()->to(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $comments   = Comment::where('post_id', $id)->delete();
        $post       = Post::findOrFail($id)->delete();

        $request->session()->flash('status', 'Post deletado com sucesso!');
        return redirect()->to(route('posts.index'));
    }

    /**
     * Export Excel CSV
     */
    public function export($postId) 
    {
        // return Excel::download(new CommentsExport($postId), 'users.xlsx');
        return (new CommentsExport($postId))->download('comentarios-'.$postId.'.csv', \Maatwebsite\Excel\Excel::CSV, ['Content-Type' => 'text/csv']);
    }
}
